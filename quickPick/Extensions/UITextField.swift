//
//  UITextField.swift
//  quickPick
//
//  Created by Balnur Sakhybekova on 25.04.2021.
//

import Foundation
import UIKit

extension UITextField {
    func setLeftView(image: UIImage, alpha: CGFloat) {
    let iconView = UIImageView(frame: CGRect(x: 13, y: 9, width: 20, height: 20))
    iconView.alpha = alpha
    iconView.image = image
    let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    iconContainerView.addSubview(iconView)
    leftView = iconContainerView
    leftViewMode = .always
    self.tintColor = UIColor(hexString: "#6D7885")
  }
}

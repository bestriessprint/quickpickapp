//
//  UIColor.swift
//  quickPick
//
//  Created by Balnur Sakhybekova on 25.04.2021.
//

import Foundation
import UIKit

extension UIColor {

//  MARK: - Private colors

    static let _gray1 = UIColor(hexString: "#2D343F")
    static let _gray2 = UIColor(hexString: "#50575A")
    static let _gray3 = UIColor(hexString: "#7F9197")
    static let _gray4 = UIColor(hexString: "#A6B6BF")
    static let _gray5 = UIColor(hexString: "#E4EAF3")
    static let _gray6 = UIColor(hexString: "#F7F9FB")

//  MARK: - Public colors

    static var orange1 = UIColor(hexString: "#FA4A0C")

    static var grayMain1 = UIColor(hexString: "#F5F5F8")

    static var grayMain2 = UIColor(hexString: "#EFEEEE")

    static var shadowColor = UIColor(hexString: "#c4c4c4")

    static var blue1 = UIColor(hexString: "#0CACEE")

    static var red1 = UIColor(hexString: "#F7685B")

    static var green1 = UIColor(hexString: "#2ED47A")

    static var yellow1 = UIColor(hexString: "#FFB946")

    static var _mainBlacky = UIColor(hexString: "161718")
    static var _darkMainBG = UIColor(hexString: "182029")
    static var _darkBG = UIColor(hexString: "11171E")
    static var _mainGood = UIColor(hexString: "0CACEE")

    

//  MARK: Init

    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64

        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }

        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }

}

//
//  SignUpViewController.swift
//  quickPick
//
//  Created by Balnur Sakhybekova on 09.04.2021.
//

import UIKit

class SignUpViewController: UIViewController {

    private lazy var upperContainerView: ContainerView = {
        let v = ContainerView()
        return v
    }()

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.alignment = .top
        sv.spacing = 12
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()

    private let logoView: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "logo")
        return v
    }()

    private let emailView: InputFieldView = {
        let v = InputFieldView()
        v.label.text = "Email"
        v.inputTextField.backgroundColor = .white
        v.inputTextField.placeholder = "example@email.com"
        v.inputTextField.setLeftView(image: UIImage(named: "icon_email")!, alpha: 0.3)
        v.inputTextField.isSecureTextEntry = false
        v.errorLabel.isHidden = true
         return v
    }()

    private let passwordView: InputFieldView = {
        let v = InputFieldView()
        v.label.text = "Password"
        v.inputTextField.placeholder = "******"
        v.inputTextField.backgroundColor = .white
        v.inputTextField.setLeftView(image: UIImage(named: "icon_password")!, alpha: 0.3)
        v.inputTextField.isSecureTextEntry = true
        v.errorLabel.isHidden = true
        v.inputTextField.textColor = .black
         return v
    }()

    private let confirmPasswordView: InputFieldView = {
        let v = InputFieldView()
        v.label.text = "Confirm password"
        v.inputTextField.placeholder = "******"
        v.inputTextField.backgroundColor = .white
        v.inputTextField.setLeftView(image: UIImage(named: "icon_password")!, alpha: 0.3)
        v.inputTextField.isSecureTextEntry = true
        v.errorLabel.isHidden = true
        v.inputTextField.textColor = .black
        return v
    }()

    private let loginButton: ButtonView = {
        let v = ButtonView()
        v.button.backgroundColor = .orange1
        v.button.layer.cornerRadius = 10
        v.button.setTitle("Log in", for: .normal)
//        v.button.addTarget(self, action: #selector(handleLogin(_:)), for: .touchUpInside)
        return v
    }()

    private let helpButton: ButtonView = {
        let v = ButtonView()
        v.button.backgroundColor = .clear
        v.button.setTitle("Forgot password?", for: .normal)
        v.button.setTitleColor(.orange1, for: .normal)
//        v.button.addTarget(self, action: #selector(openWebviewForgotPassword(_:)), for: .touchUpInside)
        return v
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        setupNavBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        hidesBottomBarWhenPushed = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


private extension SignUpViewController {

    func setupViews() {

        view.backgroundColor = UIColor(hexString: "#F2F2F2")

        view.addSubview(upperContainerView)
        view.addSubview(logoView)
        view.addSubview(stackView)
        stackView.addArrangedSubview(emailView)
        stackView.addArrangedSubview(passwordView)
        stackView.addArrangedSubview(confirmPasswordView)
        stackView.addArrangedSubview(loginButton)
        stackView.addArrangedSubview(helpButton)


    }

    func setupConstraints() {
        upperContainerView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(150)
        }

        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(upperContainerView.snp.bottom).offset(40)
            make.left.right.equalToSuperview()
        }

        emailView.snp.makeConstraints { (make) in
            make.left.right.centerX.equalToSuperview()
            make.height.equalTo(46+20+8)
            make.width.equalTo(343)
        }

        passwordView.snp.makeConstraints { (make) in
            make.left.right.centerX.equalToSuperview()
            make.height.equalTo(46+20+8)
            make.width.equalTo(343)
        }
        confirmPasswordView.snp.makeConstraints { (make) in
            make.left.right.centerX.equalToSuperview()
            make.height.equalTo(46+20+8)
            make.width.equalTo(343)
        }

        loginButton.snp.makeConstraints { (make) in
            make.left.right.centerX.equalToSuperview()
            make.height.equalTo(46)
            make.width.equalTo(343)
        }
        helpButton.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(46)
            make.width.equalTo(343)
        }

    }

    func setupNavBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }


}


//
//  ButtonView.swift
//  quickPick
//
//  Created by Balnur Sakhybekova on 25.04.2021.
//

import UIKit

class ButtonView: UIView {

    enum State {
        case loading
        case `default`
    }

    public lazy var button: UIButton = {
        let b = UIButton()
        b.titleLabel?.font = UIFont(name: "Circular Std", size: 14)
        return b
    }()

    init() {
        super.init(frame: .zero)
        addSubview(button)
        button.snp.makeConstraints { (make) in
            make.top.centerX.equalToSuperview()
            make.height.equalTo(46)
            make.width.equalTo(343)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


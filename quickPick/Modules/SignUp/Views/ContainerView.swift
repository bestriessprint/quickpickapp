//
//  ContainerView.swift
//  quickPick
//
//  Created by Balnur Sakhybekova on 25.04.2021.
//

import UIKit

class ContainerView: UIView {
     public lazy var containerView: UIView = {
        let v = UIView()
        v.layer.cornerRadius = 20
        v.backgroundColor = .white
        v.layer.masksToBounds = true
        v.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        return v
    }()

    public lazy var labelLogin: UILabel = {
        let l = UILabel()
        l.font = .boldSystemFont(ofSize: 14)
        l.textColor = .black
        l.text = "Login"
        l.textAlignment = .left
        return l
    }()

    public lazy var labelSignUp: UILabel = {
        let l = UILabel()
        l.font = .boldSystemFont(ofSize: 14)
        l.textColor = .black
        l.text = "Sign-up"
        l.textAlignment = .left
        return l
    }()
    
    public lazy var line: UIView = {
        let v = UIView()
        v.backgroundColor = .orange1
        return v
    }()

    init() {
        super.init(frame: .zero)
        addSubview(containerView)
        containerView.addSubview(labelLogin)
        containerView.addSubview(labelSignUp)
        containerView.addSubview(line)
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        labelLogin.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(92)
            make.top.equalToSuperview().offset(100)
        }
        labelSignUp.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-92)
            make.top.equalToSuperview().offset(100)
        }
        line.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-52)
            make.top.equalTo(labelSignUp.snp.bottom).offset(8)
            make.height.equalTo(3)
            make.width.equalTo(134)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

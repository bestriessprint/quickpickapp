//
//  InputFieldView.swift
//  quickPick
//
//  Created by Balnur Sakhybekova on 25.04.2021.
//

import UIKit
import SnapKit

class InputFieldView: UIView {

    enum State {
        case error
        case `default`
    }
    private var errorLabelTopConstraint: ConstraintMakerEditable!

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.alignment = .top
        sv.spacing = 4
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()

    public lazy var label: UILabel = {
        let l = UILabel()
        l.font = .boldSystemFont(ofSize: 12)
        l.textColor = UIColor(hexString: "#343B4C")
        l.textAlignment = .left
        return l
    }()

    public lazy var inputTextField: UITextField = {
        let ti = UITextField()
        ti.font = .systemFont(ofSize: 14)
        ti.layer.cornerRadius = 10
        ti.tintColor = UIColor(hexString: "#6D7885")
        ti.textColor = .black
        ti.isSecureTextEntry = true
        return ti
    }()

    public lazy var errorLabel: UILabel = {
        let l = UILabel()
        l.text = "You must enter a correct email address"
        l.textColor = .red
        l.textAlignment = .left
        l.font = .systemFont(ofSize: 12)
        return l
    }()

    private lazy var errorHolderView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    init() {
        super.init(frame: .zero)

        backgroundColor = .clear
        addSubview(stackView)
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(inputTextField)
        stackView.addArrangedSubview(errorLabel)

        stackView.snp.makeConstraints { (make) in
            make.top.centerX.equalToSuperview()
            make.width.equalTo(343)
        }

        label.snp.makeConstraints { (make) in
            make.height.equalTo(20)
            make.width.equalTo(343)
        }
        inputTextField.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.height.equalTo(46)
            make.width.equalTo(343)
        }

        errorLabel.snp.makeConstraints { (make) in
            make.height.equalTo(20)
            make.width.equalTo(343)
        }



    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showError() {
        errorLabel.isHidden = false

        inputTextField.layer.borderColor = UIColor.red.cgColor
        inputTextField.layer.borderWidth = 1
    }

    func hideError() {
        errorLabel.isHidden = true
        inputTextField.layer.borderWidth = 0
        inputTextField.layer.borderColor = .none
    }

}
